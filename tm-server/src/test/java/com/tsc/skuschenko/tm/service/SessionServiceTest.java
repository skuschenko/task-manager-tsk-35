package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.repository.ISessionRepository;
import com.tsc.skuschenko.tm.api.service.ISessionService;
import com.tsc.skuschenko.tm.bootstrap.Bootstrap;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import com.tsc.skuschenko.tm.model.Session;
import com.tsc.skuschenko.tm.repository.SessionRepository;
import com.tsc.skuschenko.tm.util.SignatureUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;

public class SessionServiceTest {

    @Test
    public void close() {
        @NotNull ISessionService sessionService = testService();
        @Nullable final Session session = sessionService.open("user1", "user1");
        Assert.assertNotNull(session);
        sessionService.close(session);
        @Nullable final Session sessionFind =
                sessionService.getSessionRepository().findById(session.getId());
        sessionService.close(session);
        Assert.assertNull(sessionFind);
    }

    @Test
    public void open() {
        @NotNull ISessionService sessionService = testService();
        @NotNull final Session session = sessionService.open("user1", "user1");
        Assert.assertNotNull(session);
        @Nullable final Session sessionFind =
                sessionService.getSessionRepository().findById(session.getId());
        Assert.assertNotNull(sessionFind);
    }

    @Test (expected = AccessForbiddenException.class)
    public void openAccessForbidden() {
        @NotNull ISessionService sessionService = testService();
        @NotNull final Session session = sessionService.open("user2", "user2");
        Assert.assertNotNull(session);
        @Nullable final Session sessionFind =
                sessionService.getSessionRepository().findById(session.getId());
        Assert.assertNotNull(sessionFind);
    }

    @Test
    public void sign() {
        @NotNull ISessionService sessionService = testService();
        @NotNull final Session session = sessionService.open("user1", "user1");
        @Nullable final String signature =
                SignatureUtil.sign(session, "salt", 2565);
        session.setSignature(signature);
        @Nullable final Session sessionFind =
                sessionService.getSessionRepository().findById(session.getId());
        Assert.assertNotNull(sessionFind);
        Assert.assertEquals(signature, session.getSignature());
    }

    @NotNull
    private ISessionService testService() {
        @NotNull final ISessionRepository sessionRepository =
                new SessionRepository();
        Assert.assertNotNull(sessionRepository);
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        Assert.assertNotNull(bootstrap);
        bootstrap.getUserService().create("user1","user1");
        @NotNull ISessionService sessionService =
                new SessionService(bootstrap, sessionRepository);
        Assert.assertNotNull(sessionService);
        return sessionService;
    }

}
