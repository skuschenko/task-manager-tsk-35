package com.tsc.skuschenko.tm.api.service;

import com.tsc.skuschenko.tm.model.Project;
import org.jetbrains.annotations.NotNull;

public interface IProjectService extends IBusinessService<Project> {

    @NotNull
    Project add(
            @NotNull String userId, @NotNull String name,
            @NotNull String description
    );

}
